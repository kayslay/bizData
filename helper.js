require("dotenv").config()

const urlsLength = require("./crawler_config/urls").length
const randomstring = require("randomstring")

switch (process.argv[2]) {
    case "url_len":
        console.log(urlsLength)
        break;
    case "mul":
        //having a problem with command $(expr arg1 * arg2)
        console.log(parseInt(process.argv[3]) * parseInt(process.argv[4]))
        break;
    case "env":
        console.log(process.env[process.argv[3]])
        break;

    default:
        console.log(randomstring.generate(12))
        break;
}