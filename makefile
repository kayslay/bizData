pod_num=3
init:
	./script/init.sh $(pod_num)
	pm2 start ecosystem.config.js --only complete

docker:
	./script/docker.sh

cleanup:
#TODO: find a way to pass the env var
	pm2 start ecosystem.config.js --only cleanup
	

destroy:
	./script/destroy.sh
	