const mongoose = require("mongoose")
const Schema = mongoose.Schema

const bizLinks = new Schema({
    links: { type: String, required: true },
    visited: {type:Boolean,default:false},
    crawlId:{type:String,required:true}
}, { timestamps: true })

bizLinks.index({links:1},{unique:true})

module.exports = mongoose.model("bizlinks",bizLinks)