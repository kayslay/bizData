const mongoose = require("mongoose")
const Schema = mongoose.Schema

//Schema of the bizList
const Bizlist = new Schema({
    company_name: { 
        type: String,
        required: true,
        alias: "companyName"
    },
    address: String,
    phone: {type:[String],required:true},
    weblink: String,
    working_hours: {
        type: [String],
        alias: "workingHours"
    },
    business_list_url: {
        type: String,
        alias: "businessListUrl"
    },
})

Bizlist.index({company_name:1,address:1},{unique:true})

module.exports = mongoose.model("bizlist", Bizlist)