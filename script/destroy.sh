pods=$(ls . |grep "pod-[[:digit:]]*-[[:digit:]]*$")
pm2 stop complete

# loop tru
for VAR in $pods
do
    echo destroying $VAR
    heroku apps:destroy -a $VAR -c $VAR
    if [ $? != 0 ]; then
		exit 1
	fi

    rm -rf $VAR ./data/${VAR}.log

    if [ $? != 0 ]; then
		exit 1
	fi
done
