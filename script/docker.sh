# use docker as pod
count=1
i=0
MONGO_URI=$(node helper.js env MONGO_URI)
DEPTH=$(node helper.js env DEPTH)
if [ $1 ]; then
	count=$1
fi

url_len=$(node helper.js url_len)
diff=$(expr $url_len / $count)

while [ $i -lt $count ]; do
	# run the docker container
	CRAWL_ID=pod_${i}
    start=$(node helper.js mul $diff $i)
    end=$(expr $start + $diff)
	echo "docker run --name pod_${i} -d  --env DEPTH=$DEPTH --env START_INDEX=$start --env END_INDEX=$end -v $(pwd)/data:/usr/src/app/data  --env MONGO_URI=$MONGO_URI --env CRAWL_ID=$CRAWL_ID crawl/bizlist"	
	docker run --name pod_${i} -d  --env DEPTH=1 --env START_INDEX=$start --env END_INDEX=$end -v $(pwd)/data:/usr/src/app/data  --env MONGO_URI=$MONGO_URI --env CRAWL_ID=$CRAWL_ID crawl/bizlist
	echo $i $start $end
	i=$(expr $i + 1)
done
