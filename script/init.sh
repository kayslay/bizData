count=1
GIT_URL="https://gitlab.com/kayslay/bizData.git"
lines="-----------------------------------------------------------------------------------"

if [ $1 ]; then
	count=$1
	echo count set to $count
fi

i=0

while [ $i -lt $count ]; do
	DIR=pod-$(date +'%M-%S')
    sleep 1
	echo "::>> starting work on $DIR"
	# git clone into a new directory
    echo $lines
	git clone $GIT_URL $DIR && cd $DIR

	if [ $? != 0 ]; then
		exit 1
	fi
	# create a new heroku project
	echo $lines      
	heroku create $DIR --no-remote

	if [ $? != 0 ]; then
		exit 1
	fi
	#git add remote
	echo $lines      
	git remote add heroku https://git.heroku.com/${DIR}.git

	if [ $? != 0 ]; then
		exit 1
	fi

	# git push
	echo $lines   
	git push heroku master

	if [ $? != 0 ]; then
		exit 1
	fi
	i=$(expr $i + 1)
    cd .. && echo $DIR >> active.pod
done

#restart the pm2 runner
# pm2 restart runner.js
