require("dotenv").config()
//mongodb setup
const mongoose = require("mongoose")
const crawler = require("web-crawljs")
//connect
mongoose.connect(process.env.MONGO_URI)
//set promise
mongoose.Promise = Promise
//bizList model
const bizListModel = require("./models/businesslist")
const bizLinkModel = require("./models/businesslistlinks")
let failedCount = 0;



//bisinesslist.com
const biz = require("./crawler_config/businesslist")
// biz(fetchFn)
switch (process.argv[2]) {
    case "complete":
        biz.log("running complete mode")
        biz.full(fetchFn)
        .then(res=>{
            console.log("crawl ending")
            setTimeout(process.exit,60000)
        })
        break;
    case "half":
        biz.log("running 1st half mode")
        biz.firstHalf()
        .then(res=>{
            console.log("crawl ending")
            setTimeout(process.exit,60000)
        })
        break;
    default:
        biz.log("running 2nd half mode")
        bizLinkModel.find({visited:false},{links:1,_id:0}).exec()
         .then((urls) => {
            const u = urls.map(u=>u.links)
            const config = biz.initConfig(fetchFn,u)
            return crawler(config).CrawlAllUrl()
         }).then(res=>{
             console.log("crawl ending")
             setTimeout(process.exit,60000)
         })
}

function fetchFn(err, data, url) {
    if (!data.company_name.length) {
        return biz.log(`${(new Date())} ERROR data is empty. could be that the ip is blocked or page does not.`)
        if (failedCount > 5) {
            killTask()
        }
        failedCount++
    }
    failedCount = 0
    let obj = {}
    obj.company_name = data.company_name[0]
    obj.address = data.address[0]
    obj.phone = (data.phone[0]) ? data.phone[0].split("<br>") : ""
    obj.weblink = data.weblink[0]
    obj.working_hours = data.working_hours
    obj.business_list_url = url
    //Todo: get the category of the company, the url and keywords
    bizListModel.create(obj, function (err, company) {
        if (err) return biz.log(`${(new Date())} ERROR ${err.message}`)
        bizLinkModel.update({links:url},{visited:true}).exec(function(err,url){
        if (err) return biz.log(`${(new Date())} ERROR ${err.message}`)
        biz.log(`${(new Date())} INFO link now visited`)        
        })
        biz.log(`${(new Date())} INFO ${company.company_name} saved successfully`)
    })
    //
}

function killTask() {
    mongoose.disconnect(function (err) {
        if (err) {
            biz.log(`${(new Date())} Error ${err.message}`)
        } else {
            biz.log(`${(new Date())} INFO ${process.env.CRAWL_ID} disconnected successfully`)
        }
        process.exit(0)
    })
}