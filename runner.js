require("dotenv").config()

const fs = require("fs")
const path = require("path")
const len = require("./crawler_config/urls").length

const {
    spawn
} = require("child_process")
console.log(process.env.MODE)

const dirs = fs.readdirSync("./")
let index = 0
dirs.forEach((dir) => {
    const _dir = path.join(__dirname, dir)
    const stat = fs.statSync(_dir)
    if (stat.isDirectory() && /^pod-/.test(dir)) {
    const env = genEnv({
        MONGO_URI:process.env.MONGO_URI,
        DEPTH:process.env.DEPTH,
        RATE_LIMIT:process.env.RATE_LIMIT,
        WAIT_LIMIT:process.env.WAIT_LIMIT,
        CRAWL_ID:dir,
        START_INDEX:index++,
        END_INDEX:index,
    })
        console.log("env variable::>",env,"for ",dir)
        console.log("--------------------------".repeat(3))
        herokuRun(_dir, env)
    }
})

/**
 * @description runs pod on heroku
 * @param {string} dir the directory of the pod
 * @param {string} env the environmental variables to pass to the node process
 */
function herokuRun(dir,env) {
    const heroku = spawn("heroku", ["run",env, "node", "index.js",(process.env.MODE?process.env.MODE:"complete")], {
        cwd: dir,
    })
    //
    const file = fs.createWriteStream(`data/${path.basename(dir)}.log`)

    // heroku.stderr.on("data", chunk => {
    //     file.write("Error:: " + chunk.toString()+"\n")
    // })

    heroku.stdout.on("data", chunk => {
        file.write("INFO:: " + chunk.toString()+"\n")
    })
}

/**
 * @description generates an env string from an object
 * @param {Object} obj 
 * @returns {String}
 */
function genEnv(obj={DEPTH:2,NODE_ENV:"heroku"}){
    let str = ""
    Object.entries(obj).forEach(v_k=>{
        str += `${v_k[0]}=${v_k[1]} `
    })
    return str
}