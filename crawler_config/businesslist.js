const crawler = require("web-crawljs")
const bizLinkModel = require("../models/businesslistlinks")
const urls = require("./urls")
const start = parseInt(process.env.START_INDEX)
const end = parseInt(process.env.END_INDEX)
const crawlId = process.env.CRAWL_ID
const fs = require("fs")


const fileLog = fs.createWriteStream(`data/${crawlId}.log`)
//log output to a file if in production. This help monitor the state of the app from the 
log = process.env.NODE_ENV == "docker" ? (...arg)=>{fileLog.write(arg.join(" ")+"\n")} : console.log

function initConfig(fetchFn, urls) {
    return {
        fetchFn,
        finalFn(err) {
            if (err) return log(`${(new Date())} ERROR ${err.message} finale`)
            log(`${(new Date())} INFO crawl finished`)
        },
        fetchSelector: {
            company_name: "span#company_name",
            address: "div.location.text",
            phone: "div.phone.text",
            weblink: "div.weblinks.text",
            working_hours: "ul.openinghours.description li"
        },
        fetchSelectBy: {
            company_name: "text",
            address: "text",
            phone: "html",
            weblink: "text",
            working_hours: "text"
        },
        nextSelectBy: {},
        nextSelector: {},
        depth: 1,
        urls: urls,
        rateLimit: 121000,
    }
}

const config = {
    fetchSelector: {
        url: "div.company.g_0 > h4 > a"
    },
    fetchSelectBy: {
        url: ["attr", "href"]
    },
    nextSelector: {
        links: "div.pages_container a.pages_no"
    },
    nextSelectBy: {
        links: ["attr", "href"]
    },
    fetchFn: (err, data, url) => {
        const _urls = (data.url.map(url => ({
            links: `https://www.businesslist.com.ng${url}`,
            crawlId:crawlId
        }))) 

        _urls.forEach(_url=>{
             bizLinkModel.create(_url, function (err, model) {
            if (err) return log(`${(new Date())} ERROR ${err.message}`) 
            log(`${(new Date())} INFO url saved successfully`)
        })
        })
       
    },
    finalFn(err) {
        if (err) log(err.message)
    },
    depth: parseInt(process.env.DEPTH),
    // limitNextLinks: 5,
    rateLimit: (parseFloat(process.env.RATE_LIMIT) * 60000),
    nextCrawlWait:(parseFloat(process.env.WAIT_LIMIT) * 60000),
    urls: urls.slice(start, end)
}

/**
 * @description gets all the links and gets all the business info
 * @param {Function} fetchFn the function to be called when the fetch operation is complete for each page
 */
module.exports.full = function (fetchFn) {
   return crawler(config).CrawlAllUrl()
    .catch(err=>log(`${(new Date())} Error ${err.message}`))
        .then(() => { //get all links that have not been visited and was added by the pod
            return bizLinkModel.find({ visited: false, crawlId }, { links: 1, _id: 0 }).exec()
        })
        .then((urls) => {
            const u = urls.map(u => u.links)
            const config = initConfig(fetchFn, u)
            return crawler(config).CrawlAllUrl()
        })
}
/**
 * @description crawls only the business links
 */
module.exports.firstHalf = crawler(config).CrawlAllUrl

module.exports.initConfig = initConfig

module.exports.log = log