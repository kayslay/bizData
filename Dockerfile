FROM node:latest
ENV NODE_ENV docker
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . .
RUN npm i 
ENTRYPOINT [ "node","index.js"]
CMD [ "complete" ]