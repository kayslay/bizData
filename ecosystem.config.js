module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [

    // First application
    {
      name: 'cleanup',
      script: 'runner.js',
      env: {
        MODE: '2nd_half'
      },
    },
    {
      name: "complete",
      script: "runner.js",
      env: {
        "MODE": "complete"
      }
    }
  ]
};